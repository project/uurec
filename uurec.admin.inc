<?php

function uurec_settings_form() {
  $form = array();
  
  $form['enabled_types'] = array(
    '#title' => t("Enabled content types"),
    '#type' => 'checkboxes',
    '#default_value' => variable_get('uurec_enabled_types', array()),
    '#options' => node_get_types('names'),
    '#description' => t('Please select which content types you want to enable as a basis to generate recommendations.')
  );
  
  $form['cron_freq'] = array(
    '#title' => t('Cron frequency'),
    '#type' => 'select',
    '#default_value' => variable_get('uurec_cron_freq', 'immediately'),
    '#options' => array(
      'immediately' => 'Immediately',
      'hourly' => t('Hourly'),
      'every6hr' => t('Every 6 hours'),
      'every12hr' => t('Every 12 hours'),
      'daily' => t('Daily'),
      'weekly' => t('Weekly')
    ),
    '#description' => t('Please specify the frequency to generate recommendation index. Note that this might be computation intensive for large site, and you have to enable Drupal cron.php.') 
  );
  
  $form['display_num'] = array(
    '#title' => t('Number of items to display'),
    '#type' => 'select',
    '#default_value' => variable_get('uurec_display_num', 5),
    '#options' => array(
      3 => '3',
      5 => '5',
      8 => '8',
      10 => '10',
      15 => '15',
    ),
    '#description' => t('Please specify how many items to show in the recommendation list.') 
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  
  return $form;
}

function uurec_settings_form_submit($form, &$form_state) {
  $cron_freq = $form_state['values']['cron_freq'];
  $display_num = $form_state['values']['display_num'];
  $enabled_types = array_diff($form_state['values']['enabled_types'], array('0'));
  
  variable_set('uurec_cron_freq', $cron_freq);
  variable_set('uurec_display_num', $display_num);
  variable_set('uurec_enabled_types', $enabled_types);
  
  drupal_set_message(t("The configuration options have been saved."));
  if ($cron_freq == 'immediately') {
    drupal_set_message(t("You have selected to build recommendation immediately. Please run cron mannually, or wait for the next cron cycle."));
  }
}