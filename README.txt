README -- User-to-user Recommendation Module

Please install Recommender API module first:
http://drupal.org/project/recommender

Configuration should be quite straight forward. If you have questions, please
submit an issue to the issue queue.
